
lib = dict()

#считать данные из файла
with open('library.txt', encoding='utf-8') as f:     
    lines = f.read().splitlines()

#записать в словарь
for line in lines:
    key, value = line.split(' - ')
    lib.update({key:value})
