#файлы
from prog import json_decode_prog
from link import json_decode_link
from sprav import json_decode_sprav
import config
import raspis
import cool
import library

#библиотеки
import random
import telebot


#создал объект-бот
bot = telebot.TeleBot(config.token)

#стартовые кнопки
kb_start = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
kb_start.add('Справочник', 'Ссылки', 'Программы', 'Расписание МИР-20-1Б', 'Тестирования', 'Расписание МИР-20-2Б')

#обработка команд
@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Привет, я бот-помощник!\nЧтобы узнать возможности отправьте /help', reply_markup = kb_start)

@bot.message_handler(commands=['help'])
def help_message(message):
    bot.send_message(message.chat.id, 'Справочник - контакты преподавателей. Ссылки - полезные ссылки на различные ресурсы.')

@bot.message_handler(commands=['stop'])
def stop_message(message):
    bot.send_message(message.chat.id, 'ok', reply_markup=None)

#обработка сообщений
@bot.message_handler(content_types=['text'])
def main_menu(message):
    if message.text == 'Справочник':
        kb_sprav = telebot.types.InlineKeyboardMarkup(row_width=1)

        for key in json_decode_sprav:
            kb_sprav.add(
                telebot.types.InlineKeyboardButton(
                    text=key,
                    callback_data=key)
            )

        bot.send_message(message.chat.id, 'Справочник:', reply_markup=kb_sprav)
    
    elif message.text == 'Ссылки':
        kb_links = telebot.types.InlineKeyboardMarkup()

        for text, url in json_decode_link:
            kb_links.add(
                telebot.types.InlineKeyboardButton(
                    text=text,
                    url=url,
                    callback_data="linked")
            )

        bot.send_message(message.chat.id, 'Ссылки:', reply_markup=kb_links)
       
    elif message.text == 'Программы':
        kb_prog = telebot.types.InlineKeyboardMarkup()

        for text, url in json_decode_prog:
            kb_prog.add(
                telebot.types.InlineKeyboardButton(
                    text=text,
                    url=url,
                    callback_data="linked")
            )
              
        
        bot.send_message(message.chat.id, 'Программы:', reply_markup=kb_prog)

    elif message.text == 'Расписание МИР-20-1Б':
        kb_raspis = telebot.types.InlineKeyboardMarkup()

        for key in raspis.ras:
             cur_but = telebot.types.InlineKeyboardButton(text=key, url=raspis.ras[key], callback_data="ras")
             kb_raspis.add(cur_but)

        file = open('2020-2021 Raspisanie ehkzamenov EHTF MIR -20-1b (vesennijj sessiya).xlsx', 'rb')
        bot.send_document(message.chat.id, file)

    elif message.text == 'Расписание МИР-20-2Б':
        kb_raspis = telebot.types.InlineKeyboardMarkup()

        for key in raspis.ras:
             cur_but = telebot.types.InlineKeyboardButton(text=key, url=raspis.ras[key], callback_data="ras")
             kb_raspis.add(cur_but)

        file = open('2020-2021 Raspisanie ehkzamenov EHTF MIR -20-2b (vesennijj sessiya).xlsx', 'rb')
        bot.send_document(message.chat.id, file)

    elif message.text == 'Тестирования':
        kb_raspis = telebot.types.InlineKeyboardMarkup()

        for key in raspis.ras:
            cur_but = telebot.types.InlineKeyboardButton(text=key, url=raspis.ras[key], callback_data="ras")
            kb_raspis.add(cur_but)

        file = open('Raspisanie_rubezhnogo_testir__2_sem_2020_21_uch_g_posle_smeny_ETF_redaktirovannoe.pdf', 'rb')
        bot.send_document(message.chat.id, file)
            


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    if call.message.text == "Справочник:":
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.id, text=call.data + '\n' + json_decode_sprav[call.data], reply_markup=None)
    elif call.data == "linked":
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.id, text="Ссылки...", reply_markup=None)
    elif call.data == "faq1":
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.id, text="Корпус А 2 этаж 215 кабинет", reply_markup=None)
    elif call.data == "faq2":
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.id, text="На каждом этаже корпуса, по левую руку от входа", reply_markup=None)

#циклическая проверка на новые сообщения боту
bot.polling()